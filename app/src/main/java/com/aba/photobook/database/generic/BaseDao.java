package com.aba.photobook.database.generic;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;

import java.util.List;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long upsert(T obj);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> upsert(List<T> obj);

    @Delete
    void delete(T obj);

}

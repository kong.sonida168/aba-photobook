package com.aba.photobook.database.generic;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.aba.photobook.base.MyApplication;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
abstract public class BaseRepository<T extends BaseEntity> {
    private   String            DB_NAME  = "photobook.aba";
    protected PhotoBookDatabase dao      = null;
    protected Executor          executor = Executors.newSingleThreadExecutor();

    public BaseRepository() {
        dao = Room.databaseBuilder(MyApplication.context, PhotoBookDatabase.class, DB_NAME).build();
    }

    abstract public void upsertTask(final T obj);

    abstract public void upsertTask(final List<T> obj);

    abstract public void deleteTask(final int id);

    abstract public void deleteTask(final T obj);

    abstract public LiveData<T> getTask(final int id);

    abstract public LiveData<List<T>> getTasks();
}

package com.aba.photobook.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.aba.photobook.database.generic.BaseDao;
import com.aba.photobook.network.model.Photo;

import java.util.List;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@Dao
abstract public class PhotoDao implements BaseDao<Photo> {

    @Query("SELECT * FROM photo")
    abstract public LiveData<List<Photo>> fetchAll();

    @Query("SELECT * FROM Photo WHERE id =:id")
    abstract public LiveData<Photo> single(int id);
}
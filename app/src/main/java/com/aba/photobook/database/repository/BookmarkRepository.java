package com.aba.photobook.database.repository;

import android.annotation.SuppressLint;

import androidx.lifecycle.LiveData;

import com.aba.photobook.database.generic.BaseRepository;
import com.aba.photobook.network.model.Bookmark;
import com.aba.photobook.utils.Log;

import java.util.Date;
import java.util.List;

import lombok.val;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class BookmarkRepository extends BaseRepository<Bookmark> {

    @SuppressLint("StaticFieldLeak")
    @Override
    public void upsertTask(final Bookmark bookmark) {
        bookmark.setCreatedAt(new Date());
        executor.execute(() -> {
            val id = dao.bookmarkDao().upsert(bookmark);
            Log.i("LOG >> upsert >> id : " + id);
        });
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void upsertTask(final List<Bookmark> bookmarks) {
        for (val bookmark : bookmarks) {
            bookmark.setCreatedAt(new Date());
        }
        executor.execute(() -> {
            val id = dao.bookmarkDao().upsert(bookmarks);
            Log.i("LOG >> upsert >> id : " + id);
        });
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void deleteTask(final int id) {
        final LiveData<Bookmark> task = getTask(id);
        if (task != null) {
            executor.execute(() -> dao.bookmarkDao().delete(task.getValue()));
        }
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void deleteTask(final Bookmark bookmark) {
        executor.execute(() -> dao.bookmarkDao().delete(bookmark));
    }

    @Override
    public LiveData<Bookmark> getTask(int id) {
        return dao.bookmarkDao().single(id);
    }

    @Override
    public LiveData<List<Bookmark>> getTasks() {
        return dao.bookmarkDao().fetchAll();
    }
}
package com.aba.photobook.database.repository;

import android.annotation.SuppressLint;

import androidx.lifecycle.LiveData;

import com.aba.photobook.database.generic.BaseRepository;
import com.aba.photobook.network.model.Photo;
import com.aba.photobook.utils.Log;

import java.util.Date;
import java.util.List;

import lombok.val;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class PhotoRepository extends BaseRepository<Photo> {

    @SuppressLint("StaticFieldLeak")
    @Override
    public void upsertTask(final Photo photo) {
        photo.setCreatedAt(new Date());
        executor.execute(() -> {
            val id = dao.photoDao().upsert(photo);
            Log.i("LOG >> upsert >> id : " + id);
        });
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void upsertTask(final List<Photo> photos) {
        for (val photo : photos) {
            photo.setCreatedAt(new Date());
        }
        executor.execute(() -> {
            val id = dao.photoDao().upsert(photos);
            Log.i("LOG >> upsert >> id : " + id);
        });
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void deleteTask(final int id) {
        final LiveData<Photo> task = getTask(id);
        if (task != null) {
            executor.execute(() -> dao.photoDao().delete(task.getValue()));
        }
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void deleteTask(final Photo photo) {
        executor.execute(() -> dao.photoDao().delete(photo));
    }

    @Override
    public LiveData<Photo> getTask(int id) {
        return dao.photoDao().single(id);
    }

    @Override
    public LiveData<List<Photo>> getTasks() {
        return dao.photoDao().fetchAll();
    }
}
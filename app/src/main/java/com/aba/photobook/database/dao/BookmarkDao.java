package com.aba.photobook.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.aba.photobook.database.generic.BaseDao;
import com.aba.photobook.network.model.Bookmark;

import java.util.List;
/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@Dao
abstract public class BookmarkDao implements BaseDao<Bookmark> {

    @Query("SELECT * FROM Bookmark")
    abstract public LiveData<List<Bookmark>> fetchAll();

    @Query("SELECT * FROM Bookmark WHERE id =:id")
    abstract public LiveData<Bookmark> single(int id);
}
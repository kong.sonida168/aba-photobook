package com.aba.photobook.database.generic;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.aba.photobook.database.dao.BookmarkDao;
import com.aba.photobook.database.dao.PhotoDao;
import com.aba.photobook.network.model.Bookmark;
import com.aba.photobook.network.model.Photo;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@Database(entities = {Photo.class, Bookmark.class}, version = 1, exportSchema = false)
public abstract class PhotoBookDatabase extends RoomDatabase {

    public abstract PhotoDao photoDao();

    public abstract BookmarkDao bookmarkDao();
}
package com.aba.photobook.network.base;

import okhttp3.Request;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class HttpHeader implements AuthorizationHandler {

    private static final String KEY_TOKEN        = "x-api-token";
    private static final String KEY_LANG         = "x-user-lang";
    private static final String KEY_CONTENT_TYPE = "Content-Type";
    private static final String KEY_LOGIN_TOKEN  = "x-login-token";

    private static final String DEFAULT_CONTENT_TYPE = "application/json";

    public static final String LANG_EN = "en_US";
    public static final String LANG_FR = "fr_CA";

    private String token;
    private String lang;
    private String contentType;
    private String loginToken;

    public HttpHeader(String token) {
        this(token, "", "en", DEFAULT_CONTENT_TYPE);
    }

    /*public HttpHeader(String token, String lang) {
        this(token, "", lang, DEFAULT_CONTENT_TYPE);
    }*/

    public HttpHeader(String token, String loginToken) {
        this(token, loginToken, "en", DEFAULT_CONTENT_TYPE);
    }

    public HttpHeader(String token, String loginToken, String lang, String contentType) {
        this.token = token;
        this.loginToken = loginToken;
        this.lang = lang;
        this.contentType = contentType;
    }

    @Override
    public Request applyAuthorizationHeader(Request request) {
        return request.newBuilder()
                .addHeader(KEY_TOKEN, token)
                .addHeader(KEY_LANG, lang)
                .addHeader(KEY_CONTENT_TYPE, contentType)
                .addHeader(KEY_LOGIN_TOKEN, loginToken)
                .build();
    }

    @Override
    public void onUnAuthorized() {

    }

    @Override
    public String getTrackingUUID() {
        return null;
    }
}

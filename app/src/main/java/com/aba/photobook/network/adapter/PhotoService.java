package com.aba.photobook.network.adapter;

import com.aba.photobook.network.model.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public interface PhotoService {

    @GET("v2/list")
    Call<List<Photo>> list(@Query("page") int page, @Query("limit") int limit);

}

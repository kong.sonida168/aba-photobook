package com.aba.photobook.network.response;

import com.aba.photobook.network.model.BaseGson;

import lombok.Getter;
import lombok.Setter;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@Setter
@Getter
public class ResponseListPhoto extends BaseGson {

}

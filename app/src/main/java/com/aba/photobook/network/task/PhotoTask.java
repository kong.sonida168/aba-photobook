package com.aba.photobook.network.task;


import com.aba.photobook.network.adapter.PhotoService;
import com.aba.photobook.network.base.BaseNetwork;
import com.aba.photobook.network.base.HttpHeader;
import com.aba.photobook.network.request.RequestListPhotoParam;

import lombok.val;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class PhotoTask extends BaseNetwork<RequestListPhotoParam, Response> {

    public PhotoTask(RequestListPhotoParam param) {
        super(new HttpHeader(BaseNetwork.getToken()));
        setData(param);
    }

    @Override
    protected Response onExecute(Retrofit retrofit, RequestListPhotoParam param) throws Exception {
        val service = retrofit.create(PhotoService.class);
        val call    = service.list(param.getPage(), param.getLimit());
        return call.execute();
    }
}

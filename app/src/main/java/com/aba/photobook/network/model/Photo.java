package com.aba.photobook.network.model;

import androidx.room.Entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@Setter
@Getter
@Entity(tableName = "photo")
public class Photo extends Bookmark {
}

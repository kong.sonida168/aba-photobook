package com.aba.photobook.network.model;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.loader.content.CursorLoader;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.aba.photobook.BuildConfig;
import com.aba.photobook.base.MyApplication;
import com.aba.photobook.database.generic.BaseEntity;
import com.aba.photobook.utils.TimestampConverter;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.val;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@Setter
@Getter
@Entity(tableName = "bookmark")
public class Bookmark extends BaseEntity {

    public static final  String EXTRA              = "Bookmark";
    private static final String SPECIFIC_IMAGE_URL = BuildConfig.BASE_URL + "id/%s/%d/%d";

    @PrimaryKey
    @SerializedName("id")
    @ColumnInfo(name = "id")
    @NonNull
    private String id;

    @SerializedName("author")
    @ColumnInfo(name = "author")
    private String author;

    @SerializedName("width")
    @ColumnInfo(name = "width")
    private int width;

    @SerializedName("height")
    @ColumnInfo(name = "height")
    private int height;

    @SerializedName("url")
    @ColumnInfo(name = "url")
    private String url;

    @SerializedName("download_url")
    @ColumnInfo(name = "download_url")
    private String downloadUrl;

    @SerializedName("gallery_uri")
    @ColumnInfo(name = "gallery_uri")
    private String galleryUri;

    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")
    @TypeConverters({TimestampConverter.class})
    private Date createdAt;

    public String getThumbnail(int width, int height) {
        return String.format(SPECIFIC_IMAGE_URL, id, width, height);
    }

    public String getDefaultThumbnail() {
        return String.format(SPECIFIC_IMAGE_URL, id, 200, 200);
    }

    public String getThumbnail(int percentSize) {
        val w = (width / 100) * percentSize;
        val h = (height / 100) * percentSize;
        return String.format(SPECIFIC_IMAGE_URL, id, w, h);
    }

    public String getRealPathGalleryFromURI() {
        if (StringUtils.isEmpty(galleryUri)) {
            return null;
        }
        String[]     proj         = {MediaStore.Images.Media.DATA};
        CursorLoader loader       = new CursorLoader(MyApplication.context, Uri.parse(galleryUri), proj, null, null, null);
        Cursor       cursor       = loader.loadInBackground();
        int          column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }
}

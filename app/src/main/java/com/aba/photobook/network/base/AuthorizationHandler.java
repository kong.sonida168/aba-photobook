package com.aba.photobook.network.base;

import okhttp3.Request;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public interface AuthorizationHandler {

    public Request applyAuthorizationHeader(Request request);

    public void onUnAuthorized();

    public String getTrackingUUID();

}

package com.aba.photobook.network.base;

import com.aba.photobook.BuildConfig;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class ConstNetwork {
    public static String BASE_URL = BuildConfig.BASE_URL;
}

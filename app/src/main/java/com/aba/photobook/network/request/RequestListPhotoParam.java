package com.aba.photobook.network.request;

import com.aba.photobook.network.model.BaseGson;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@Data
@AllArgsConstructor
public class RequestListPhotoParam extends BaseGson {

    @SerializedName("page")
    private int page  = 1;  // default
    @SerializedName("limit")
    private int limit = 30; // default

    public RequestListPhotoParam increasePage() {
        page += 1;
        return this;
    }
}

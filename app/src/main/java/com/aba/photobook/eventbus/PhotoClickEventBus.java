package com.aba.photobook.eventbus;

import com.aba.photobook.network.model.BaseGson;
import com.aba.photobook.network.model.Bookmark;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@Data
@AllArgsConstructor
public class PhotoClickEventBus extends BaseGson {
    private Bookmark photo;
}

package com.aba.photobook.eventbus;

import com.aba.photobook.network.model.BaseGson;

import lombok.Data;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@Data
public class ReloadBookmarkEventBus extends BaseGson {
}

package com.aba.photobook.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aba.photobook.databinding.ItemPhotoBinding;
import com.aba.photobook.network.model.Bookmark;
import com.aba.photobook.network.model.Photo;
import com.aba.photobook.ui.adapter.viewholder.PhotoHolder;

import java.util.ArrayList;
import java.util.List;

import lombok.val;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class PhotoAdapter extends RecyclerView.Adapter<PhotoHolder> {
    private Context        context = null;
    private List<Bookmark> photos  = new ArrayList<>();

    public PhotoAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public PhotoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        val binding = ItemPhotoBinding.inflate(LayoutInflater.from(context), parent, false);
        return new PhotoHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoHolder holder, int position) {
        holder.bind(photos.get(position));
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void addBookmark(List<Bookmark> photos) {
        this.photos.addAll(photos);
    }

    public void addPhotos(List<Photo> photos) {
        this.photos.addAll(photos);
    }

    public void clearData() {
        photos.clear();
    }
}

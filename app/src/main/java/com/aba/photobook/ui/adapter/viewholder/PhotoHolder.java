package com.aba.photobook.ui.adapter.viewholder;

import androidx.recyclerview.widget.RecyclerView;

import com.aba.photobook.databinding.ItemPhotoBinding;
import com.aba.photobook.eventbus.PhotoClickEventBus;
import com.aba.photobook.network.model.Bookmark;

import org.greenrobot.eventbus.EventBus;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class PhotoHolder extends RecyclerView.ViewHolder {
    private ItemPhotoBinding binding;

    public PhotoHolder(ItemPhotoBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Bookmark item) {
        binding.image.setImageURI(item.getDefaultThumbnail());

        binding.getRoot().setOnClickListener(__ -> EventBus.getDefault().postSticky(new PhotoClickEventBus(item)));
    }
}

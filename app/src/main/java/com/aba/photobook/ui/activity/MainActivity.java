package com.aba.photobook.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.aba.photobook.R;
import com.aba.photobook.base.BaseActivity;
import com.aba.photobook.eventbus.OpenBookmarkEventBus;
import com.aba.photobook.eventbus.PhotoClickEventBus;
import com.aba.photobook.ui.fragment.BookmarkFragment;
import com.aba.photobook.ui.fragment.PhotoFragment;
import com.aba.photobook.ui.fragment.PreviewPhotoFragment;
import com.aba.photobook.utils.Log;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import lombok.val;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class MainActivity extends BaseActivity {

    public static void open(Context context) {
        val intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction().replace(R.id.content, new PhotoFragment()).commit();
    }

    @Override
    public void trackScreenView() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPhotoClickEventBus(PhotoClickEventBus event) {
        Log.i("LOG >> EvenBus : " + event);
        val fragment = PreviewPhotoFragment.newInstance(event.getPhoto());
        getSupportFragmentManager().beginTransaction().add(R.id.content, fragment).addToBackStack(null).commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOpenBookmarkEventBus(OpenBookmarkEventBus event) {
        Log.i("LOG >> EvenBus : " + event);
        val fragment = new BookmarkFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.content, fragment).addToBackStack(null).commit();
    }
}

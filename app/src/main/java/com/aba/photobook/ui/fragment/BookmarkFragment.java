package com.aba.photobook.ui.fragment;

import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.aba.photobook.R;
import com.aba.photobook.base.BaseFragment;
import com.aba.photobook.database.repository.BookmarkRepository;
import com.aba.photobook.databinding.FragmentBookmarksBinding;
import com.aba.photobook.eventbus.ReloadBookmarkEventBus;
import com.aba.photobook.network.model.Bookmark;
import com.aba.photobook.ui.adapter.PhotoAdapter;
import com.aba.photobook.utils.Log;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import lombok.val;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class BookmarkFragment extends BaseFragment<FragmentBookmarksBinding> {

    private PhotoAdapter             adapter;
    private LiveData<List<Bookmark>> liveData;

    @Override
    public void setupUI() {
        binding.layoutToolbar.txtTitle.setText(R.string.bookmarks);
        adapter = new PhotoAdapter(getContext());
        binding.layoutRecyclerView.setupUI(3, null);
        binding.layoutRecyclerView.setAdapter(adapter);
        binding.layoutRecyclerView.addOnScrollListener(onScrollListener);
        loadingData();
    }

    private void loadingData() {
        binding.layoutRecyclerView.showProgressBar();
        val repo = new BookmarkRepository();
        liveData = repo.getTasks();
        liveData.removeObservers(this);
        liveData.observe(this, nameObserver);
    }

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            Log.i("dy : " + dy + "   dx : " + dx);
            if (dy == 0) {
                ViewCompat.setElevation(binding.layoutToolbar.appBar, 0);
            }
            else if (dy < 10) {
                ViewCompat.setElevation(binding.layoutToolbar.appBar, getContext().getResources().getDimension(R.dimen.toolbar_shadow));
            }
        }
    };

    private final Observer<List<Bookmark>> nameObserver = new Observer<List<Bookmark>>() {

        @Override
        public void onChanged(@Nullable final List<Bookmark> photos) {
            Log.i("LOG >> photos >> " + photos);
            if (photos != null) {
                Log.i("LOG >> photos >> " + photos.size());
                adapter.clearData();
                adapter.addBookmark(photos);
                adapter.notifyDataSetChanged();
                binding.layoutRecyclerView.checkList();
            }
            binding.layoutRecyclerView.hideProgressBar();
        }
    };

    @Override
    protected int layoutId() {
        return R.layout.fragment_bookmarks;
    }

    @Override
    protected void trackScreenView() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReloadBookmarkEventBus(ReloadBookmarkEventBus event) {
        Log.i("LOG >> EvenBus : " + event);
        new Handler().postDelayed(() -> loadingData(), 400);
    }
}

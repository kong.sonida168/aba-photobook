package com.aba.photobook.ui.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.aba.photobook.R;
import com.aba.photobook.base.BaseFragment;
import com.aba.photobook.database.repository.PhotoRepository;
import com.aba.photobook.databinding.FragmentPhotoBinding;
import com.aba.photobook.eventbus.OpenBookmarkEventBus;
import com.aba.photobook.network.model.Photo;
import com.aba.photobook.network.request.RequestListPhotoParam;
import com.aba.photobook.network.task.PhotoTask;
import com.aba.photobook.ui.adapter.PhotoAdapter;
import com.aba.photobook.ui.view.RecyclerViewHelper;
import com.aba.photobook.utils.ApplicationUtil;
import com.aba.photobook.utils.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import lombok.val;
import retrofit2.Response;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class PhotoFragment extends BaseFragment<FragmentPhotoBinding> {

    private RequestListPhotoParam photoParam = new RequestListPhotoParam(1, 50);

    private PhotoAdapter adapter;

    @Override
    public void setupUI() {
        binding.layoutToolbar.txtTitle.setText(R.string.app_name);
        adapter = new PhotoAdapter(getContext());
        binding.layoutRecyclerView.setupUI(3, new RecyclerViewHelper.Callback() {

            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.i("onLoadMore >> page : " + page + "    totalItemCount : " + totalItemsCount);
                photoParam.increasePage();
                loadingData();
            }
        });
        binding.layoutRecyclerView.setAdapter(adapter);
        binding.layoutRecyclerView.addOnScrollListener(onScrollListener);
        binding.floatActionBookmarks.setOnClickListener(__ -> EventBus.getDefault().postSticky(new OpenBookmarkEventBus()));
        loadingData();
    }

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                binding.floatActionBookmarks.show();
            }
            else {
                binding.floatActionBookmarks.hide();
            }
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            Log.i("dy : " + dy + "   dx : " + dx);
            if (dy == 0) {
                ViewCompat.setElevation(binding.layoutToolbar.appBar, 0);
            }
            else if (dy < 10) {
                ViewCompat.setElevation(binding.layoutToolbar.appBar, getContext().getResources().getDimension(R.dimen.toolbar_shadow));
            }
        }
    };

    private void loadingData() {
        binding.layoutRecyclerView.showProgressBar();
        if (ApplicationUtil.isOnline(getContext())) {
            val task = new PhotoTask(photoParam);
            getCompositeDisposable().add(task.start(task.new SimpleObserver() {
                @Override
                public void onReceiveResult(RequestListPhotoParam request, Response result) throws Exception {
                    super.onReceiveResult(request, result);
                    if (result.isSuccessful()) {
                        val data       = (ArrayList<Photo>) result.body();
                        val repository = new PhotoRepository();
                        repository.upsertTask(data);
                    }
                }

                @Override
                public void onNext(Response result) {
                    Log.i("LOG >> onNext >> result : " + result);
                    if (result.isSuccessful()) {
                        val data = (ArrayList<Photo>) result.body();
                        if (data != null && data.size() > 0) {
                            adapter.addPhotos(data);
                            adapter.notifyDataSetChanged();
                            binding.layoutRecyclerView.checkList();
                        }
                    }
                    binding.layoutRecyclerView.hideProgressBar();
                }

                @Override
                public void onError(Throwable e) {
                    Log.e("LOG >> onError >> result : " + e);
                    binding.layoutRecyclerView.hideProgressBar();
                }
            }));
        }
        else {
            val repo = new PhotoRepository();
            repo.getTasks().observe(this, nameObserver);
        }
    }

    private final Observer<List<Photo>> nameObserver = new Observer<List<Photo>>() {

        @Override
        public void onChanged(@Nullable final List<Photo> photos) {
            if (photos != null && photos.size() > 0) {
                adapter.addPhotos(photos);
                adapter.notifyDataSetChanged();
                binding.layoutRecyclerView.checkList();
            }
            binding.layoutRecyclerView.hideProgressBar();
        }
    };

    @Override
    protected int layoutId() {
        return R.layout.fragment_photo;
    }

    @Override
    protected void trackScreenView() {

    }
}

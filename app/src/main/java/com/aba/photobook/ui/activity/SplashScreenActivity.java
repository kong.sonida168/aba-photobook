package com.aba.photobook.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;

import com.aba.photobook.R;
import com.aba.photobook.base.BaseActivity;
import com.aba.photobook.databinding.ActivitySplashscreenBinding;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class SplashScreenActivity extends BaseActivity {


    private ActivitySplashscreenBinding binding  = null;
    private int                         DELAY    = 1_000;
    private Handler                     handler  = new Handler();
    private Runnable                    runnable = () -> {
        finish();
        if (isAlive) {
            MainActivity.open(this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splashscreen);

        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, DELAY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.shimmerContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        binding.shimmerContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void trackScreenView() {

    }
}

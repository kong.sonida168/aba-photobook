package com.aba.photobook.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.aba.photobook.R;
import com.aba.photobook.base.BaseFragment;
import com.aba.photobook.database.repository.BookmarkRepository;
import com.aba.photobook.databinding.FragmentPreviewPhotoBinding;
import com.aba.photobook.eventbus.ReloadBookmarkEventBus;
import com.aba.photobook.network.model.Bookmark;
import com.aba.photobook.ui.dialog.DialogProgressLoading;
import com.aba.photobook.utils.Log;
import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.indicator.progresspie.ProgressPieIndicator;
import com.github.piasy.biv.loader.ImageLoader;
import com.github.piasy.biv.view.FrescoImageViewFactory;
import com.github.piasy.biv.view.ImageSaveCallback;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;

import java.io.File;

import lombok.val;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
@RuntimePermissions
public class PreviewPhotoFragment extends BaseFragment<FragmentPreviewPhotoBinding> {

    private       Bookmark              photo           = null;
    private       DialogProgressLoading progressLoading = null;
    private final int                   PERCENT_SIZE    = 70;
    private final boolean               ORIGINAL        = false;

    public static PreviewPhotoFragment newInstance(@NonNull Bookmark photo) {
        val fragment = new PreviewPhotoFragment();
        val bundle   = new Bundle();
        bundle.putSerializable(Bookmark.EXTRA, photo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setupUI() {
        binding.floatActionSave.hide();
        progressLoading = new DialogProgressLoading(getContext());

        photo = (Bookmark) getArguments().getSerializable(Bookmark.EXTRA);
        if (photo == null) {
            throw new RuntimeException("Please call method newInstance");
        }
        Log.i("LOG >> " + photo.toPrettyJson());

        binding.bigImage.setImageViewFactory(new FrescoImageViewFactory());
        binding.bigImage.setImageLoaderCallback(loaderCallback);
        binding.bigImage.setImageSaveCallback(saveCallback);
        binding.bigImage.setProgressIndicator(new ProgressPieIndicator());
        downloadImage();
    }

    private void downloadImage() {
        val uriThumb    = Uri.parse(photo.getDefaultThumbnail());
        val uriDownload = Uri.parse(ORIGINAL ? photo.getDownloadUrl() : photo.getThumbnail(PERCENT_SIZE));

        if (StringUtils.isNotEmpty(photo.getRealPathGalleryFromURI())) {
            val file = new File(photo.getRealPathGalleryFromURI());
            if (file.exists()) {
                val uri = Uri.fromFile(file);
                binding.bigImage.showImage(uriThumb, uri);
                return;
            }
        }
        binding.bigImage.showImage(uriThumb, uriDownload);
    }

    private void checkVisibleButtons() {
        // Study case delete or save button (bookmark case show delete button)
        if (StringUtils.isEmpty(photo.getGalleryUri())) {
            binding.floatActionSave.setVisibility(View.VISIBLE);
            binding.floatActionDelete.setVisibility(View.GONE);
        }
        else {
            binding.floatActionSave.setVisibility(View.GONE);
            binding.floatActionDelete.setVisibility(View.VISIBLE);
        }

        binding.floatActionSave.setOnClickListener(__ -> PreviewPhotoFragmentPermissionsDispatcher.saveImageWithPermissionCheck(this));
        binding.floatActionDelete.setOnClickListener(__ -> askDelete());
    }

    private ImageLoader.Callback loaderCallback = new ImageLoader.Callback() {

        @Override
        public void onCacheHit(int imageType, File image) {
            Log.i("LOG >> onCacheHit " + image);
        }

        @Override
        public void onCacheMiss(int imageType, File image) {
            Log.i("LOG >> onCacheMiss " + image);
        }

        @Override
        public void onStart() {
            Log.i("LOG >> onStart");
            binding.floatActionSave.hide();
            binding.floatActionDelete.hide();
        }

        @Override
        public void onProgress(int progress) {
            Log.i("LOG >> onProgress " + progress);
        }

        @Override
        public void onFinish() {
            Log.i("LOG >> onFinish");
        }

        @Override
        public void onSuccess(File image) {
            Log.i("LOG >> onSuccess " + image);
            checkVisibleButtons();
        }

        @Override
        public void onFail(Exception error) {
            Log.e("LOG >> onFinish");
        }
    };

    private ImageSaveCallback saveCallback = new ImageSaveCallback() {
        @Override
        public void onSuccess(String uri) {
            Log.i("LOG >> onSuccess " + uri);
            photo.setGalleryUri(uri);
            val repository = new BookmarkRepository();
            repository.upsertTask(photo);
            getActivity().runOnUiThread(() -> Toast.makeText(getContext(), R.string.msg_save_image_success, Toast.LENGTH_LONG).show());
        }

        @Override
        public void onFail(Throwable t) {
            Log.e("LOG >> onFail " + t);
            getActivity().runOnUiThread(() -> Toast.makeText(getContext(), R.string.msg_save_image_failed, Toast.LENGTH_LONG).show());
        }
    };

    @Override
    protected int layoutId() {
        return R.layout.fragment_preview_photo;
    }

    @Override
    protected void trackScreenView() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressLoading != null) {
            progressLoading.dismiss();
        }
        BigImageViewer.imageLoader().cancelAll();
    }

    @SuppressLint("StaticFieldLeak")
    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void saveImage() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressLoading.show();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressLoading.hide();
            }

            @SuppressLint("MissingPermission")
            @Override
            protected Void doInBackground(Void... voids) {
                binding.bigImage.saveImageIntoGallery();
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void showRationaleForImage(PermissionRequest request) {
        showRationaleDialog(R.string.permission_storage_rationale, request);
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void onImageDenied() {
        Toast.makeText(getContext(), R.string.permission_storage_denied, Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void onImageNeverAskAgain() {
        Toast.makeText(getContext(), R.string.permission_storage_never_ask_again, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PreviewPhotoFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void showRationaleDialog(@StringRes int messageResId, final PermissionRequest request) {
        new AlertDialog.Builder(getContext())
                .setPositiveButton(R.string.button_allow, (dialog, which) -> request.proceed())
                .setNegativeButton(R.string.button_deny, (dialog, which) -> request.cancel())
                .setCancelable(false)
                .setMessage(messageResId)
                .show();
    }

    private void askDelete() {
        new AlertDialog.Builder(getContext())
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    val file = new File(photo.getRealPathGalleryFromURI());
                    file.delete();
                    val repo = new BookmarkRepository();
                    repo.deleteTask(photo);
                    EventBus.getDefault().postSticky(new ReloadBookmarkEventBus());
                    getActivity().getSupportFragmentManager().popBackStack();
                })
                .setNegativeButton(R.string.cancel, null)
                .setMessage(R.string.ask_delete_image_desc)
                .setIcon(R.mipmap.ic_delete_black_24dp)
                .show();
    }
}

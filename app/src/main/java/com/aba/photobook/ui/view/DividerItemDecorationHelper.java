package com.aba.photobook.ui.view;

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.aba.photobook.R;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class DividerItemDecorationHelper extends DividerItemDecoration {
    public DividerItemDecorationHelper(Context context) {
        super(context, DividerItemDecoration.VERTICAL);
        setDrawable(ContextCompat.getDrawable(context, R.drawable.recycler_divider));
    }
}

package com.aba.photobook.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aba.photobook.R;


/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class DialogProgressLoading extends Dialog {


    public DialogProgressLoading(@NonNull Context context) {
        super(context);
        init();
    }

    public DialogProgressLoading(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    protected DialogProgressLoading(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init();
    }

    private void init() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_progress);
    }
}

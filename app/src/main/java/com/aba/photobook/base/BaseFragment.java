package com.aba.photobook.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.aba.photobook.eventbus.FakeEventBus;
import com.aba.photobook.utils.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.disposables.CompositeDisposable;
import lombok.Setter;
/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public abstract class BaseFragment<T extends ViewDataBinding> extends Fragment {
    @Setter
    private   boolean             enabledDefaultSubscribeEventBus = true;
    protected T                   binding                         = null;
    private   CompositeDisposable compositeDisposable             = null;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFakeEventBus(FakeEventBus event) {
        Log.i("LOG >> EvenBus : " + event);
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trackScreenView();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (layoutId() == 0) {
            throw new RuntimeException("You need to setup layout ID");
        }
        binding = DataBindingUtil.inflate(inflater, layoutId(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupUI();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (enabledDefaultSubscribeEventBus) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (enabledDefaultSubscribeEventBus) {
            EventBus.getDefault().unregister(this);
        }
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
    }

    /**
     * This method will be called from onActivityCreated()
     */
    abstract protected void setupUI();

    abstract protected int layoutId();

    abstract protected void trackScreenView();
}

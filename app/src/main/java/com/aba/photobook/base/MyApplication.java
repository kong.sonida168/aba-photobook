package com.aba.photobook.base;

import android.app.Application;
import android.content.Context;

import com.aba.photobook.BuildConfig;
import com.aba.photobook.network.base.BaseNetwork;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;
import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.loader.fresco.FrescoImageLoader;

import io.fabric.sdk.android.Fabric;
import okhttp3.logging.HttpLoggingInterceptor;
/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class MyApplication extends Application {

    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        Fabric.with(this, new Crashlytics(), new Answers());
        Fresco.initialize(this);

        BigImageViewer.initialize(FrescoImageLoader.with(this));

        if (BuildConfig.DEBUG) {
            BaseNetwork.attachInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
//            Stetho.initialize(
//                    Stetho.newInitializerBuilder(this)
//                            .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
//                            .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
//                            .build());

            Stetho.initializeWithDefaults(this);
        }
    }
}

package com.aba.photobook.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aba.photobook.R;
import com.aba.photobook.eventbus.FakeEventBus;
import com.aba.photobook.utils.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import lombok.Setter;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected Context context                         = this;
    @Setter
    public    boolean isAlive                         = false;
    @Setter
    private   boolean enabledDefaultSubscribeEventBus = true;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFakeEventBus(FakeEventBus event) {
        Log.i("LOG >> EvenBus : " + event);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.fade_in_quick, R.anim.fade_out_quick);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        trackScreenView();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (enabledDefaultSubscribeEventBus) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (enabledDefaultSubscribeEventBus) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isAlive = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAlive = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isAlive = false;
    }

    public abstract void trackScreenView();
}

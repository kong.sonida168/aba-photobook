package com.aba.photobook.utils;

import android.text.TextUtils;

import com.aba.photobook.BuildConfig;

import lombok.val;
import lombok.var;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Sonida Kong
 * kongsonida168@gmail.com
 */
public class Log {
    private static final String TAG = "ABA";

    public static void i(Object msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.i(TAG, getLocation() + msg);
        }
    }

    public static void d(Object msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.d(TAG, getLocation() + msg);
        }
    }

    public static void w(Object msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.w(TAG, getLocation() + msg);
        }
    }

    public static void i(Throwable throwable) {
        i(throwable.getMessage());
    }

    public static void e(Object msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.e(TAG, getLocation() + msg);
        }
    }

    public static void e(Throwable throwable) {
        if (BuildConfig.DEBUG) {
            android.util.Log.e(TAG, getLocation() + throwable.getMessage());
        }
    }

    public static void e(Throwable throwable, Response response) {
        String url = response.raw().request().url().toString();
        e(throwable.getMessage() + "\nURL: " + url);
    }

    public static void e(Throwable throwable, Call call) {
        String url = call.request().toString();
        e(throwable.getMessage() + "\nURL: " + url);
    }

    private static String getLocation() {
        val className = Log.class.getName();
        val traces    = Thread.currentThread().getStackTrace();
        var found     = false;

        for (StackTraceElement trace : traces) {
            try {
                if (found) {
                    if (!trace.getClassName().startsWith(className)) {
                        val clazz = Class.forName(trace.getClassName());
                        return "[" + getClassName(clazz) + ":" + trace.getMethodName() + ":" + trace.getLineNumber() + "]: ";
                    }
                }
                else if (trace.getClassName().startsWith(className)) {
                    found = true;
                }
            } catch (ClassNotFoundException ignored) {
            }
        }

        return "[]: ";
    }

    private static String getClassName(Class<?> clazz) {
        if (clazz != null) {
            if (!TextUtils.isEmpty(clazz.getSimpleName())) {
                return clazz.getSimpleName();
            }

            return getClassName(clazz.getEnclosingClass());
        }

        return "";
    }
}
# Photo book of ABA
### Installation
- Install lombok in Android Studio https://projectlombok.org/setup/android

### Libraries are used in this project
|Library|Description|
|---|---|
|`AndroidX & Material design`|Android library|
|`android.arch.persistence.room:runtime`|CRUD data in database|
|`com.github.piasy:BigImageViewer`|View big image|
|`com.crashlytics.sdk.android:crashlytics`|Track crash report (Fabric.io)|
|`com.facebook.stetho:stetho`|View database or sources from ChromeBrowser|
|`org.greenrobot:eventbus`|Broadcast event from fragment to fragment, activity to activity...etc|
|`com.google.code.gson:gson`|Convert json string to json object or object to json string|
|`com.facebook.stetho:stetho`|View database or sources from ChromeBrowser|
|`Retrofit, okhttp3`|Request network|
|`io.reactivex.rxjava2:rxandroid`|Handle process (request) in background and foreground|
|`com.facebook.fresco:fresco`|Load image from server to ImageView and cached|
|`com.facebook.shimmer:shimmer`|Effect in splashscreen...etc|
|`org.projectlombok:lombok`|Make fast development for generating object (Getter, Setter...etc)|
|`org.permissionsdispatcher:permissionsdispatcher`|Handle request permission by using annotation|

### Developer
|Name|Email|Linkedin|
|---|---|---|
|**Sonida Kong**|kongsonida168@gmail.com|https://www.linkedin.com/in/kongsonida|

### License
```
Source code is copyright by Sonida Kong (Developer)
```